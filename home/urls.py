from django.urls import path

from . import views

app_name = 'home'

urlpatterns = [
    path('', views.index, name='index'),
    path('isian/', views.isian, name='isian'),
    path('isian/isiProvinsi/', views.isiProv, name='isiProv'),
    path('isian/isiUniversitas/', views.isiUniv, name='isiUniv'),
    path('signout/', views.signout, name ='signout'),
    path('lihatuniv/', views.lihatuniv, name='lihatuniv'),
    path('lihatprov/', views.lihatprov, name='lihatprov'),
]