from django.db import models

# Create your models here.
class Provinsi(models.Model):
    provinsi = models.CharField('Provinsi', max_length = 100, null = True)

class Universitas(models.Model):
    nama = models.CharField('Nama', max_length = 100, null = True)
    provinsi = models.CharField('Provinsi', max_length = 100, null = True)