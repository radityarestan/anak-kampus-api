from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from .forms import ProvinsiForm, UniversitasForm
from .models import Universitas, Provinsi
from django.http import HttpResponseRedirect, JsonResponse

# Create your views here.
def index(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(request, username=username, password=password)

        if user is not None:
            login(request, user)
            return HttpResponseRedirect('isian/')

        else :
            messages.error(request,'*username or password not correct')


    return render(request, 'index.html')

def isian(request):
    provinsiForm = ProvinsiForm()
    universitasForm = UniversitasForm()
    return render(request, 'isian.html', {'provform' : provinsiForm, 'univform' : universitasForm})

def isiProv(request):
    provForm = ProvinsiForm(request.POST)
    if provForm.is_valid():
        provForm.save()
    return HttpResponseRedirect('/isian/')

def isiUniv(request):
    univform = UniversitasForm(request.POST)
    if univform.is_valid():
        univform.save()
    return HttpResponseRedirect('/isian/')

def lihatprov(request):
    all_prov = Provinsi.objects.all().values()
    return JsonResponse({'result' : list(all_prov)})

def lihatuniv(request):
    all_univ = Universitas.objects.all().values()
    return JsonResponse({'result' : list(all_univ)})

def signout(request):
    logout(request)
    return redirect('/')