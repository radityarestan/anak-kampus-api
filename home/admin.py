from django.contrib import admin
from .models import Provinsi, Universitas

# Register your models here.
admin.site.register(Provinsi)
admin.site.register(Universitas)