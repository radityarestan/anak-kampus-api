from django.forms import ModelForm
from .models import Provinsi, Universitas

class ProvinsiForm(ModelForm):
    required_css_class = 'required'

    class Meta:
        model = Provinsi
        fields = "__all__" 

class UniversitasForm(ModelForm):
    required_css_class = 'required'

    class Meta:
        model = Universitas
        fields = "__all__" 